rootProject.name = "kotka"
include("kotka-shared")
include("kotka-server")
include("kotka-consumer")
include("kotka-producer")
