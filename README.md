# Fun with Coroutines: Kotka

Demo repository showcasing some Kotlin Coroutines features in a fake messaging system, Kotka. Kotka is NOT intended for production use!

The repository consists of:

- `kotka-server`: The messaging Spring WebFlux server instance
- `kotka-producer`: A producer command-line application that generates messages
- `kotka-consumer`: A consumer command-line application that receives messages
- `kotka-shared`: Library containing some common Kotka functionality

The applications can be run either by starting the `main` function in the application classes, or by using Gradle, e.g.:

```kotlin
gradlew kotka-server:bootRun
```
