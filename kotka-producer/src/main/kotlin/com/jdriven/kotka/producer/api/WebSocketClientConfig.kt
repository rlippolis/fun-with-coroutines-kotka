package com.jdriven.kotka.producer.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.jdriven.kotka.shared.api.producer.KotkaProducerMessageConverter
import com.jdriven.kotka.shared.websockets.client.KotkaWebSocketClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class WebSocketClientConfig {

    @Bean
    fun kotkaProducerMessageConverter(objectMapper: ObjectMapper) = KotkaProducerMessageConverter(objectMapper)

    @Bean
    fun kotkaProducerWebSocketClient(
        kotkaProducerMessageConverter: KotkaProducerMessageConverter,
    ) = KotkaWebSocketClient(kotkaProducerMessageConverter)
}
