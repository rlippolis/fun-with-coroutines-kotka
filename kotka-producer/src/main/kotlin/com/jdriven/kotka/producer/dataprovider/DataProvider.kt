package com.jdriven.kotka.producer.dataprovider

import kotlinx.coroutines.flow.flow
import net.datafaker.AbstractProvider
import net.datafaker.Faker
import kotlin.random.Random

object DataProvider {
    private val faker = Faker()

    val beer = infiniteFlowWith(faker.beer()) {
        """${name()}: ${hop()} ${malt()} ${style()}"""
    }

    val bigBangTheory = infiniteFlowWith(faker.bigBangTheory()) {
        """${character()}: "${quote()}""""
    }

    val brooklynNineNine = infiniteFlowWith(faker.brooklynNineNine()) {
        """${characters()}: "${quotes()}""""
    }

    val chuckNorrisFacts = infiniteFlowWith(faker.chuckNorris()) { fact() }

    private inline fun <T: AbstractProvider> infiniteFlowWith(faker: T, crossinline elementProvider: T.() -> String) =
        flow {
            with (faker) {
                while (true) {
                    emit(elementProvider())
                }
            }
        }
}
