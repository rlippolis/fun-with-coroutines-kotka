package com.jdriven.kotka.producer.config

import com.jdriven.kotka.producer.dataprovider.DataProvider
import kotlin.random.Random
import kotlin.time.Duration.Companion.milliseconds

object KotkaProducerConfig {

    val producers = mapOf(
        "big-bang-theory" to DataProvider.bigBangTheory,
        "beer" to DataProvider.beer,
        "b99" to DataProvider.brooklynNineNine,
        "chuck-norris" to DataProvider.chuckNorrisFacts,
    )

    val randomDelay get() = Random.nextInt(250, 1000).milliseconds
}
