package com.jdriven.kotka.producer

import com.jdriven.kotka.producer.config.KotkaProducerConfig
import com.jdriven.kotka.shared.api.producer.KotkaProducerClientMessage
import com.jdriven.kotka.shared.api.producer.KotkaProducerServerMessage
import com.jdriven.kotka.shared.util.ClientIdMDCContext
import com.jdriven.kotka.shared.websockets.client.KotkaWebSocketClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class KotkaProducerRunner(
    private val kotkaProducerWebSocketClient: KotkaWebSocketClient<KotkaProducerServerMessage, KotkaProducerClientMessage>,
): CommandLineRunner {

    override fun run(vararg args: String) = runBlocking {
        KotkaProducerConfig.producers.forEach { (topic, dataProvider) ->
            launchProducer(
                topic = topic,
                dataProvider = dataProvider,
            )
        }
    }

    context(CoroutineScope)
    private fun launchProducer(topic: String, dataProvider: Flow<String>) {
        val clientId = "producer-$topic"

        launch(ClientIdMDCContext(clientId)) {
            kotkaProducerWebSocketClient.connect(
                url = "ws://localhost:8080/produce",
                clientId = clientId,
                serverMessageConsumer = {
                    log.info("Consumed: $it")
                },
                clientMessageProducer = dataProvider
                    .map { KotkaProducerClientMessage(topic = topic, payload = it) }
                    .onEach { delay(KotkaProducerConfig.randomDelay) }
                    .onEach { log.info("""Produced: "${it.payload}"""") },
            )
        }
    }

    private val log = LoggerFactory.getLogger(this::class.java)
}
