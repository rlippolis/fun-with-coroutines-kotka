package com.jdriven.kotka.producer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotkaProducerApplication

fun main(args: Array<String>) {
    runApplication<KotkaProducerApplication>(*args)
}
