package com.jdriven.kotka.shared.api

object KotkaConnectionApiHeaders {
    const val clientId = "X-KOTKA-CLIENT-ID"
}
