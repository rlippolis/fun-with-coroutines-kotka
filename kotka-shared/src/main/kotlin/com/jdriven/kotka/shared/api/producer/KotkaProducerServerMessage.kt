package com.jdriven.kotka.shared.api.producer

import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
sealed interface KotkaProducerServerMessage {
    data class Text(val text: String): KotkaProducerServerMessage
}
