package com.jdriven.kotka.shared.api.consumer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.jdriven.kotka.shared.websockets.client.WebSocketClientMessageConverter
import com.jdriven.kotka.shared.websockets.server.WebSocketServerMessageConverter

class KotkaConsumerMessageConverter(
    private val objectMapper: ObjectMapper,
): WebSocketServerMessageConverter<KotkaConsumerServerMessage, KotkaConsumerClientMessage>,
    WebSocketClientMessageConverter<KotkaConsumerServerMessage, KotkaConsumerClientMessage> {

    override fun convertClientMessage(input: String): KotkaConsumerClientMessage = objectMapper.readValue(input)
    override fun convertClientMessage(input: KotkaConsumerClientMessage): String = objectMapper.writeValueAsString(input)

    override fun convertServerMessage(input: String): KotkaConsumerServerMessage = objectMapper.readValue(input)
    override fun convertServerMessage(input: KotkaConsumerServerMessage): String = objectMapper.writeValueAsString(input)
}
