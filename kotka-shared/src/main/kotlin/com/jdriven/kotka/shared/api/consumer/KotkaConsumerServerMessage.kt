package com.jdriven.kotka.shared.api.consumer

data class KotkaConsumerServerMessage(val topic: String, val payload: String)
