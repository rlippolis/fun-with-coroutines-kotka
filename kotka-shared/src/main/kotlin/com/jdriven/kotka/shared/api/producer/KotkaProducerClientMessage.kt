package com.jdriven.kotka.shared.api.producer

import com.fasterxml.jackson.annotation.JsonTypeInfo

data class KotkaProducerClientMessage(val topic: String, val payload: String)
