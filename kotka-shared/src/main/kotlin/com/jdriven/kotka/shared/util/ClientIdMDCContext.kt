package com.jdriven.kotka.shared.util

import kotlinx.coroutines.ThreadContextElement
import org.slf4j.MDC
import kotlin.coroutines.CoroutineContext

/**
 * Populates a Kotlin Coroutines context with the `clientId` in the MDC context.
 *
 * Note: this is just to show the inner workings of a ThreadContextElement, i.e. binding thread-local values to
 * coroutine contexts. If you actually need your logging MDC context to be populated in your own project,
 * consider using the [kotlinx-coroutines-slf4j](https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-slf4j/) library.
 * This library contains a readily-available `MDCContext`, which will generically work with all the items in your MDC context.
 */
class ClientIdMDCContext(private val clientId: String): ThreadContextElement<String?> {
    companion object Key : CoroutineContext.Key<ClientIdMDCContext>

    override val key: CoroutineContext.Key<ClientIdMDCContext> get() = Key

    override fun updateThreadContext(context: CoroutineContext): String? =
        MDC.get(clientIdMDCKey).also { MDC.put(clientIdMDCKey, clientId) }

    override fun restoreThreadContext(context: CoroutineContext, oldState: String?) =
        oldState?.let { MDC.put(clientIdMDCKey, it) } ?: MDC.remove(clientIdMDCKey)

    private val clientIdMDCKey = "clientId"
}
