package com.jdriven.kotka.shared.websockets.client

interface WebSocketClientMessageConverter<S, C> {

    fun convertClientMessage(input: C): String

    fun convertServerMessage(input: String): S
}
