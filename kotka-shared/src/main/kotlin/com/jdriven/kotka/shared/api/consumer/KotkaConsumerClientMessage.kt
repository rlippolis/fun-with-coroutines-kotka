package com.jdriven.kotka.shared.api.consumer

import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
sealed interface KotkaConsumerClientMessage {
    data class SubscribeToTopic(val topic: String): KotkaConsumerClientMessage
    data class UnsubscribeFromTopic(val topic: String): KotkaConsumerClientMessage
}
