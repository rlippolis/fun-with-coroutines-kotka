package com.jdriven.kotka.shared.websockets.client

import com.jdriven.kotka.shared.api.KotkaConnectionApiHeaders
import com.jdriven.kotka.shared.util.ClientIdMDCContext
import kotlinx.coroutines.CoroutineStart.LAZY
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactor.asFlux
import kotlinx.coroutines.reactor.awaitSingleOrNull
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.web.reactive.socket.WebSocketSession
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient
import reactor.core.publisher.Mono
import java.net.URI

class KotkaWebSocketClient<S, C>(
    private val webSocketClientMessageConverter: WebSocketClientMessageConverter<S, C>,
) {

    suspend fun connect(
        url: String,
        clientId: String,
        serverMessageConsumer: (S) -> Unit = {},
        clientMessageProducer: Flow<C> = emptyFlow(),
    ) = withContext(ClientIdMDCContext(clientId)) {
        val httpHeaders = HttpHeaders().apply {
            add(KotkaConnectionApiHeaders.clientId, clientId)
        }

        // TODO this feels like a possibly unnecessary indirection,
        //  can we directly link the serverMessageConsumer / clientMessageProducer to the websocket in-/output?
        val serverMessageChannel = Channel<S>()
        val clientMessageChannel = Channel<C>()

        // Consume incoming messages
        val receiveJob = launch(start = LAZY) { serverMessageChannel.consumeEach(serverMessageConsumer) }

        // Produce outgoing messages
        val sendJob = launch(start = LAZY) { clientMessageProducer.collect { clientMessageChannel.send(it) } }

        // Connect to the websocket server
        val connectionJob = launch {
            nettyWebSocketClient.execute(URI(url), httpHeaders) { session ->
                runBlocking(ClientIdMDCContext(clientId)) { log.info("Connected to server") } // TODO this is ugly
                receiveJob.start()
                sendJob.start()
                val webSocketInput = createWebSocketInput(session, serverMessageChannel)
                val webSocketOutput = createWebSocketOutput(session, clientMessageChannel)

                Mono.zip(webSocketInput, webSocketOutput)
                    .then()
            }.awaitSingleOrNull().also {
                log.info("Disconnected from server")
                receiveJob.cancel()
                sendJob.cancel()
            }
        }

        connectionJob.join()
    }

    private fun createWebSocketInput(session: WebSocketSession, serverMessageChannel: Channel<S>) =
        session.receive()
            .map { it.payloadAsText }
            .map { message -> webSocketClientMessageConverter.convertServerMessage(message) }
            .flatMap { message -> mono { serverMessageChannel.send(message) } }
            .then()

    private fun createWebSocketOutput(session: WebSocketSession, clientMessageChannel: Channel<C>) =
        clientMessageChannel
            .consumeAsFlow()
            .map { message -> webSocketClientMessageConverter.convertClientMessage(message) }
            .map { message -> session.textMessage(message) }
            .asFlux()
            .let {
                session.send(it)
            }

    private val log = LoggerFactory.getLogger(this::class.java)

    companion object {
        private val nettyWebSocketClient = ReactorNettyWebSocketClient()
    }
}
