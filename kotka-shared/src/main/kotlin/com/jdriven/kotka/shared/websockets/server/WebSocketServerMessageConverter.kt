package com.jdriven.kotka.shared.websockets.server

interface WebSocketServerMessageConverter<S, C> {

    fun convertClientMessage(input: String): C

    fun convertServerMessage(input: S): String
}
