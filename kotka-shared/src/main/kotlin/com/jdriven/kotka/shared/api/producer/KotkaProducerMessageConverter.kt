package com.jdriven.kotka.shared.api.producer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.jdriven.kotka.shared.websockets.client.WebSocketClientMessageConverter
import com.jdriven.kotka.shared.websockets.server.WebSocketServerMessageConverter

class KotkaProducerMessageConverter(
    private val objectMapper: ObjectMapper,
): WebSocketServerMessageConverter<KotkaProducerServerMessage, KotkaProducerClientMessage>,
    WebSocketClientMessageConverter<KotkaProducerServerMessage, KotkaProducerClientMessage> {

    override fun convertClientMessage(input: String): KotkaProducerClientMessage = objectMapper.readValue(input)
    override fun convertClientMessage(input: KotkaProducerClientMessage): String = objectMapper.writeValueAsString(input)

    override fun convertServerMessage(input: String): KotkaProducerServerMessage = objectMapper.readValue(input)
    override fun convertServerMessage(input: KotkaProducerServerMessage): String = objectMapper.writeValueAsString(input)
}
