package com.jdriven.kotka.server.service

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.BUFFERED
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.seconds

/**
 * This class shows how to do unit testing with coroutines.
 *
 * @see <a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-test/">the official documentation</a>
 */
@OptIn(ExperimentalCoroutinesApi::class)
internal class KotkaMessageProcessorTest {

    // Use the `runTest` function to run coroutine-scoped code in a test
    @Test
    fun `Should be able to receive messages from a subscribed topic`() = runTest {
        val subject = KotkaMessageProcessor()
        val consumerChannel = Channel<String>(capacity = BUFFERED)

        subject.subscribeToTopic(consumerName = testConsumer, topic = testTopic, channel = consumerChannel) { it }

        subject.produceMessage(topic = testTopic, message = testMessage1)
        subject.produceMessage(topic = testTopic, message = testMessage2)

        advanceUntilIdle() // Fast-forward to the point where all coroutines have done their thing

        subject.unsubscribeFromTopic(consumerName = testConsumer, topic = testTopic)

        assertThat(consumerChannel.receive()).isEqualTo(testMessage1)
        assertThat(consumerChannel.receive()).isEqualTo(testMessage2)
    }

    // Note that even though we use delays of multiple seconds here, the test finishes in milliseconds,
    // because the time inside the unit test is virtual
    @Test
    fun `Should stop receiving messages after unsubscribing from a topic`() = runTest {
        val subject = KotkaMessageProcessor()
        val consumerChannel = Channel<String>(capacity = BUFFERED)

        // Subscribe to the topic for 10 seconds
        launch {
            subject.subscribeToTopic(consumerName = testConsumer, topic = testTopic, channel = consumerChannel) { it }
            delay(10_000)
            subject.unsubscribeFromTopic(consumerName = testConsumer, topic = testTopic)
        }

        // Produce 2 messages, the first one after 5 seconds and the second one after another 30 seconds
        launch {
            delay(5.seconds)
            subject.produceMessage(topic = testTopic, message = testMessage1)
            delay(30.seconds)
            subject.produceMessage(topic = testTopic, message = testMessage2)
        }

        advanceUntilIdle() // Fast-forward to the point where all coroutines have done their thing

        // Only one message should be consumed
        assertThat(consumerChannel.receive()).isEqualTo(testMessage1)
        assertThat(consumerChannel.tryReceive().isFailure).isTrue()
    }

    @Test
    fun `Should buffer the last 100 messages, as that is the topic retention size`() = runTest {
        val subject = KotkaMessageProcessor()
        val consumerChannel = Channel<String>(capacity = 1000) // Use a large enough capacity

        // Produce 200 numbered messages
        for (i in 1..200) {
            subject.produceMessage(topic = testTopic, message = "$i")
        }
        advanceUntilIdle()

        // Subscribe, wait until all buffered messages have been received, then unsubscribe
        subject.subscribeToTopic(consumerName = testConsumer, topic = testTopic, channel = consumerChannel) { it }
        advanceUntilIdle()
        subject.unsubscribeFromTopic(consumerName = testConsumer, topic = testTopic)

        // Close the channel, otherwise the next method will hang indefinitely (actually: until the test suite times out)
        consumerChannel.close()
        // Collect all messages sent to the channel into a list
        val receivedMessages = consumerChannel.consumeAsFlow().toCollection(mutableListOf()).map { it.toInt() }

        // We expect only the last 100 elements
        assertThat(receivedMessages).containsExactlyElementsOf(101..200)
    }

    companion object {
        private const val testTopic = "test-topic"
        private const val testConsumer = "test-consumer"

        private const val testMessage1 = "test-msg-1"
        private const val testMessage2 = "test-msg-2"
    }
}
