package com.jdriven.kotka.server.service

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap

@Service
class KotkaMessageProcessor {

    private val topicFlows = ConcurrentHashMap<String, MutableSharedFlow<String>>()

    private val topicSubscriptions = ConcurrentHashMap<TopicConsumer, Job>()

    suspend fun produceMessage(topic: String, message: String) = flowFor(topic).emit(message)

    context(CoroutineScope)
    fun <T> subscribeToTopic(
        consumerName: String,
        topic: String,
        channel: SendChannel<T>,
        transform: (String) -> T,
    ) {
        topicSubscriptions.compute(TopicConsumer(consumerName = consumerName, topic = topic)) { _, existing ->
            log.info("Subscribing to topic '$topic'")
            check(existing == null || existing.isCompleted) { "Consumer already subscribed to topic!" }

            launch {
                flowFor(topic)
                    .map(transform)
                    .collect(channel::send)
            }
        }
    }

    fun unsubscribeFromTopic(consumerName: String, topic: String) {
        log.info("Unsubscribing from topic '$topic'")
        topicSubscriptions.remove(TopicConsumer(consumerName = consumerName, topic = topic))
            ?.cancel()
    }

    private fun flowFor(topic: String): MutableSharedFlow<String> =
        topicFlows.computeIfAbsent(topic) {
            MutableSharedFlow(replay = TOPIC_RETENTION) // TODO we can configure buffering here
        }

    private val log = LoggerFactory.getLogger(this::class.java)

    companion object {
        private const val TOPIC_RETENTION = 100
    }
}

private data class TopicConsumer(val consumerName: String, val topic: String)
