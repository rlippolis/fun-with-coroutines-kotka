package com.jdriven.kotka.server.api

import com.jdriven.kotka.shared.api.KotkaConnectionApiHeaders
import com.jdriven.kotka.shared.util.ClientIdMDCContext
import com.jdriven.kotka.shared.websockets.server.WebSocketServerMessageConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactor.asFlux
import kotlinx.coroutines.reactor.awaitSingleOrNull
import kotlinx.coroutines.reactor.mono
import org.slf4j.LoggerFactory
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.Mono

class KotkaWebSocketHandler<S, C>(
    private val webSocketServerMessageConverter: WebSocketServerMessageConverter<S, C>,
    private val webSocketConnectionListener: WebSocketConnectionListener<S, C>,
) : WebSocketHandler {

    override fun handle(session: WebSocketSession): Mono<Void> {
        val clientId = session.requiredHeader(KotkaConnectionApiHeaders.clientId)
        return mono(ClientIdMDCContext(clientId)) {
            log.info("Client connected")

            val serverMessageProducer = Channel<S>()
            val clientMessageConsumer = Channel<C>()

            val webSocketInput = createWebSocketInput(session, clientMessageConsumer)
            val webSocketOutput = createWebSocketOutput(session, serverMessageProducer)

            webSocketConnectionListener.onWebSocketConnect(
                clientId = clientId,
                connection = KotkaWebSocketConnection(clientMessageConsumer, serverMessageProducer)
            )

            Mono.zip(webSocketInput, webSocketOutput)
                .then()
                .awaitSingleOrNull().also {
                    log.info("Client disconnected")
                    webSocketConnectionListener.onWebSocketDisconnect(clientId = clientId)
                }
        }
    }

    private fun createWebSocketInput(session: WebSocketSession, clientMessageConsumer: Channel<C>) =
        session.receive()
            .map { it.payloadAsText }
            .map { webSocketServerMessageConverter.convertClientMessage(it) }
            .onErrorContinue { throwable, _ -> log.error("Unable to process incoming message", throwable) }
            .flatMap { mono { clientMessageConsumer.send(it) } }
            .then()

    private fun createWebSocketOutput(session: WebSocketSession, serverMessageProducer: Channel<S>) =
        serverMessageProducer
            .consumeAsFlow()
            .map { message -> webSocketServerMessageConverter.convertServerMessage(message) }
            .catch { throwable -> log.error("Unable to process outgoing message", throwable) }
            .map { message -> session.textMessage(message) }
            .asFlux()
            .let { session.send(it) }

    private fun WebSocketSession.requiredHeader(headerName: String): String =
        checkNotNull(handshakeInfo.headers[headerName]?.firstOrNull()?.takeIf { it.isNotBlank() }) {
            "Required header '$headerName' missing or blank!"
        }

    private val log = LoggerFactory.getLogger(this::class.java)
}

interface WebSocketConnectionListener<S, C> {
    context(CoroutineScope)
    fun onWebSocketConnect(clientId: String, connection: KotkaWebSocketConnection<S, C>) {}

    context(CoroutineScope)
    fun onWebSocketDisconnect(clientId: String) {}
}

data class KotkaWebSocketConnection<S, R>(
    val messageConsumer: ReceiveChannel<R>,
    val messageProducer: SendChannel<S>,
)
