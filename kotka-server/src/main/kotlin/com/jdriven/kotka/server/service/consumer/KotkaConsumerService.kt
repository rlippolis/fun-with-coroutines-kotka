package com.jdriven.kotka.server.service.consumer

import com.jdriven.kotka.server.api.KotkaWebSocketConnection
import com.jdriven.kotka.server.api.WebSocketConnectionListener
import com.jdriven.kotka.server.service.KotkaMessageProcessor
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerClientMessage
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerServerMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

private typealias KotkaConsumerConnection = KotkaWebSocketConnection<KotkaConsumerServerMessage, KotkaConsumerClientMessage>

@Service
class KotkaConsumerService(
    private val kotkaMessageProcessor: KotkaMessageProcessor,
): WebSocketConnectionListener<KotkaConsumerServerMessage, KotkaConsumerClientMessage> {

    context(CoroutineScope)
    override fun onWebSocketConnect(clientId: String, connection: KotkaConsumerConnection) {
        launch {
            for (message in connection.messageConsumer) {
                onConsumerMessage(clientId, message, connection)
            }
        }
    }

    context(CoroutineScope)
    private fun onConsumerMessage(consumerName: String, message: KotkaConsumerClientMessage, connection: KotkaConsumerConnection) {
        when (message) {
            is KotkaConsumerClientMessage.SubscribeToTopic -> {
                kotkaMessageProcessor.subscribeToTopic(
                    consumerName = consumerName,
                    topic = message.topic,
                    channel = connection.messageProducer
                ) { msgText ->
                    log.info("""Sending to consumer: "$msgText"""")
                    KotkaConsumerServerMessage(topic = message.topic, payload = msgText)
                }
            }

            is KotkaConsumerClientMessage.UnsubscribeFromTopic -> {
                kotkaMessageProcessor.unsubscribeFromTopic(consumerName = consumerName, topic = message.topic)
            }
        }
    }

    private val log = LoggerFactory.getLogger(this::class.java)
}
