package com.jdriven.kotka.server.service.producer

import com.jdriven.kotka.server.api.KotkaWebSocketConnection
import com.jdriven.kotka.server.api.WebSocketConnectionListener
import com.jdriven.kotka.server.service.KotkaMessageProcessor
import com.jdriven.kotka.shared.api.producer.KotkaProducerClientMessage
import com.jdriven.kotka.shared.api.producer.KotkaProducerServerMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

private typealias KotkaProducerConnection = KotkaWebSocketConnection<KotkaProducerServerMessage, KotkaProducerClientMessage>

@Service
class KotkaProducerService(
    private val kotkaMessageProcessor: KotkaMessageProcessor,
): WebSocketConnectionListener<KotkaProducerServerMessage, KotkaProducerClientMessage> {

    context(CoroutineScope)
    override fun onWebSocketConnect(clientId: String, connection: KotkaProducerConnection) {
        launch {
            for (message in connection.messageConsumer) {
                onProducerMessage(message)
            }
        }
    }

    private suspend fun onProducerMessage(message: KotkaProducerClientMessage) {
        log.info("""Received from producer: "${message.payload}"""")
        kotkaMessageProcessor.produceMessage(topic = message.topic, message = message.payload)
    }

    private val log = LoggerFactory.getLogger(this::class.java)
}
