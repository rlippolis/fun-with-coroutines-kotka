package com.jdriven.kotka.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotkaServerApplication

fun main(args: Array<String>) {
	runApplication<KotkaServerApplication>(*args)
}
