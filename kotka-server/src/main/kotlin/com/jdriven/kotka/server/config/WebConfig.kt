package com.jdriven.kotka.server.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.jdriven.kotka.server.api.KotkaWebSocketHandler
import com.jdriven.kotka.server.service.consumer.KotkaConsumerService
import com.jdriven.kotka.server.service.producer.KotkaProducerService
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerClientMessage
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerMessageConverter
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerServerMessage
import com.jdriven.kotka.shared.api.producer.KotkaProducerClientMessage
import com.jdriven.kotka.shared.api.producer.KotkaProducerMessageConverter
import com.jdriven.kotka.shared.api.producer.KotkaProducerServerMessage
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping

@Configuration
@EnableWebFlux
class WebConfig {

    @Bean
    fun kotkaConsumerMessageConverter(objectMapper: ObjectMapper) = KotkaConsumerMessageConverter(objectMapper)

    @Bean
    fun kotkaConsumerWebSocketHandler(
        kotkaConsumerMessageConverter: KotkaConsumerMessageConverter,
        kotkaConsumerService: KotkaConsumerService,
    ) = KotkaWebSocketHandler(
        webSocketServerMessageConverter = kotkaConsumerMessageConverter,
        webSocketConnectionListener = kotkaConsumerService,
    )

    @Bean
    fun kotkaProducerMessageConverter(objectMapper: ObjectMapper) = KotkaProducerMessageConverter(objectMapper)

    @Bean
    fun kotkaProducerWebSocketHandler(
        kotkaProducerMessageConverter: KotkaProducerMessageConverter,
        kotkaProducerService: KotkaProducerService,
    ) = KotkaWebSocketHandler(
        webSocketServerMessageConverter = kotkaProducerMessageConverter,
        webSocketConnectionListener = kotkaProducerService,
    )

    @Bean
    fun handlerMapping(
        kotkaConsumerWebSocketHandler: KotkaWebSocketHandler<KotkaConsumerServerMessage, KotkaConsumerClientMessage>,
        kotkaProducerWebSocketHandler: KotkaWebSocketHandler<KotkaProducerServerMessage, KotkaProducerClientMessage>,
    ): HandlerMapping = SimpleUrlHandlerMapping().apply {
        order = -1
        urlMap = mapOf(
            "/consume" to kotkaConsumerWebSocketHandler,
            "/produce" to kotkaProducerWebSocketHandler,
        )
    }
}
