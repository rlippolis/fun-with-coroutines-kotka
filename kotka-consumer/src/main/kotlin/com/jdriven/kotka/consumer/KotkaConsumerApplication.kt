package com.jdriven.kotka.consumer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotkaConsumerApplication

fun main(args: Array<String>) {
    runApplication<KotkaConsumerApplication>(*args)
}
