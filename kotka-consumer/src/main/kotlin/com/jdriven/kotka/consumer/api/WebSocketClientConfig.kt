package com.jdriven.kotka.consumer.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerMessageConverter
import com.jdriven.kotka.shared.websockets.client.KotkaWebSocketClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class WebSocketClientConfig {

    @Bean
    fun kotkaConsumerMessageConverter(objectMapper: ObjectMapper) = KotkaConsumerMessageConverter(objectMapper)

    @Bean
    fun kotkaConsumerWebSocketClient(
        kotkaConsumerMessageConverter: KotkaConsumerMessageConverter,
    ) = KotkaWebSocketClient(kotkaConsumerMessageConverter)
}
