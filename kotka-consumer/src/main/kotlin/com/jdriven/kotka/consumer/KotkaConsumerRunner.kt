package com.jdriven.kotka.consumer

import com.jdriven.kotka.consumer.config.KotkaConsumerConfig
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerClientMessage
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerClientMessage.SubscribeToTopic
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerClientMessage.UnsubscribeFromTopic
import com.jdriven.kotka.shared.api.consumer.KotkaConsumerServerMessage
import com.jdriven.kotka.shared.util.ClientIdMDCContext
import com.jdriven.kotka.shared.websockets.client.KotkaWebSocketClient
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import kotlin.time.Duration

@Component
class KotkaConsumerRunner(
    private val kotkaConsumerWebSocketClient: KotkaWebSocketClient<KotkaConsumerServerMessage, KotkaConsumerClientMessage>,
) : CommandLineRunner {

    override fun run(vararg args: String) = runBlocking {
        supervisorScope {
            KotkaConsumerConfig.consumers.forEach { (topic, subscriptionDuration) ->
                launchConsumer(topic = topic, subscriptionDuration = subscriptionDuration)
            }
        }
    }

    context(CoroutineScope)
    private fun launchConsumer(topic: String, subscriptionDuration: Duration) {
        val clientId = "consumer-$topic"

        launch(ClientIdMDCContext(clientId) + consumerExceptionHandler()) {
            kotkaConsumerWebSocketClient.connect(
                url = "ws://localhost:8080/consume",
                clientId = clientId,
                serverMessageConsumer = { message ->
                    log.info("""Consumed: "${message.payload}"""")
                },
                clientMessageProducer = flow {
                    log.info("Subscribing to topic '$topic'")
                    emit(SubscribeToTopic(topic = topic))

                    delay(subscriptionDuration)

                    log.info("Unsubscribing from topic '$topic'")
                    emit(UnsubscribeFromTopic(topic = topic))
                },
            )
        }
    }

    private fun consumerExceptionHandler() = CoroutineExceptionHandler { _, exception ->
        log.error("Stopping consumer due to exception with message: ${exception.message}")
    }

    private val log = LoggerFactory.getLogger(this::class.java)
}
