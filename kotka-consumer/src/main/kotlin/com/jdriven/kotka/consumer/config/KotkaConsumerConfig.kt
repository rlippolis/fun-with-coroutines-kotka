package com.jdriven.kotka.consumer.config

import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

object KotkaConsumerConfig {

    val consumers = mapOf(
        "b99" to 45.minutes,
        "beer" to Duration.INFINITE,
        "big-bang-theory" to 1.hours,
        "chuck-norris" to Duration.INFINITE,
    )
}
